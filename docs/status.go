package docs

import "gitlab.com/noleeen/geo_courier/module/courierfacade/models"

// swagger:route Get /api/status courier  courierListRequest
// Статуc курьера.
//
// responses:
//	 200: courierListRequest

// swagger:response courierListRequest
type courierListRequest struct {
	// in:body
	Body models.CourierStatus
}
