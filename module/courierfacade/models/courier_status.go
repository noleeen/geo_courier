package models

import (
	"gitlab.com/noleeen/geo_courier/module/courier/models"
	models2 "gitlab.com/noleeen/geo_courier/module/order/models"
)

type CourierStatus struct {
	Courier models.Courier  `json:"courier"`
	Orders  []models2.Order `json:"orders"`
}
