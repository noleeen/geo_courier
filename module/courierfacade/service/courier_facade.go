package service

import (
	"context"
	cservice "gitlab.com/noleeen/geo_courier/module/courier/service"
	"gitlab.com/noleeen/geo_courier/module/courierfacade/models"
	oservice "gitlab.com/noleeen/geo_courier/module/order/service"
	"log"
)

const (
	CourierVisibilityRadius = 2800 // 2.8km
)

type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) models.CourierStatus   // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}

func (c *CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	err = c.courierService.MoveCourier(*courier, direction, zoom)
	if err != nil {
		log.Println(err)
		return
	}
}

func (c *CourierFacade) GetStatus(ctx context.Context) models.CourierStatus {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println(err)
		return models.CourierStatus{}
	}

	orders, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierVisibilityRadius, "m")
	if err != nil {
		log.Println(err)
		return models.CourierStatus{}
	}

	return models.CourierStatus{
		Courier: *courier,
		Orders:  orders,
	}
}
