package service

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/noleeen/geo_courier/module/courier/models"
	mocks2 "gitlab.com/noleeen/geo_courier/module/courier/service/mocks"
	m "gitlab.com/noleeen/geo_courier/module/courierfacade/models"
	models2 "gitlab.com/noleeen/geo_courier/module/order/models"
	"gitlab.com/noleeen/geo_courier/module/order/service/mocks"
	"reflect"

	"testing"
)

func TestCourierFacade_MoveCourier(t *testing.T) {
	type fields struct {
		courierService *mocks2.Courierer
		orderService   *mocks.Orderer
	}
	type args struct {
		ctx       context.Context
		direction int
		zoom      int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "",
			fields: fields{
				courierService: mocks2.NewCourierer(t),
				orderService:   mocks.NewOrderer(t),
			},
			args: args{
				ctx:       context.Background(),
				direction: 0,
				zoom:      0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.courierService.
				On("GetCourier", tt.args.ctx).
				Return(&models.Courier{}, nil)
			tt.fields.courierService.
				On("MoveCourier", models.Courier{}, tt.args.direction, tt.args.zoom).
				Return(nil)

			c := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			c.MoveCourier(tt.args.ctx, tt.args.direction, tt.args.zoom)
		})
	}
}

func TestCourierFacade_GetStatus(t *testing.T) {
	type fields struct {
		courierService *mocks2.Courierer
		orderService   *mocks.Orderer
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   m.CourierStatus
	}{
		{
			name: "",
			fields: fields{
				courierService: mocks2.NewCourierer(t),
				orderService:   mocks.NewOrderer(t),
			},
			args: args{ctx: context.Background()},
			want: m.CourierStatus{
				Courier: models.Courier{},
				Orders:  []models2.Order{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.courierService.On("GetCourier", tt.args.ctx).Return(&models.Courier{}, nil)
			tt.fields.orderService.On("GetByRadius", tt.args.ctx, 0.0, 0.0, mock.Anything, "m").Return([]models2.Order{}, nil)

			c := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			if got := c.GetStatus(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}
