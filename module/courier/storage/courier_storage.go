package storage

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"gitlab.com/noleeen/geo_courier/module/courier/models"
)

//go:generate go run github.com/vektra/mockery/v2@v2.38.0 --name=CourierStorager
type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	storage *redis.Client
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
	return &CourierStorage{storage: storage}
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	marshal, err := json.Marshal(courier)
	if err != nil {
		return err
	}

	err = c.storage.Set(ctx, "courier", marshal, 0).Err()

	return err
}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	result, err := c.storage.Get(ctx, "courier").Bytes()
	if err != nil {
		return nil, err
	}

	var courier models.Courier

	err = json.Unmarshal(result, &courier)
	if err != nil {
		return nil, err
	}
	return &courier, nil
}
