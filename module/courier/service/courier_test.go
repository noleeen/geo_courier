package service

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/noleeen/geo_courier/geo"
	mocks2 "gitlab.com/noleeen/geo_courier/geo/mocks"
	"gitlab.com/noleeen/geo_courier/module/courier/models"
	"gitlab.com/noleeen/geo_courier/module/courier/storage/mocks"
	"reflect"
	"testing"
)

func TestCourierService_GetCourier(t *testing.T) {
	type fields struct {
		courierStorage *mocks.CourierStorager
		allowedZone    *mocks2.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.Courier
		wantErr bool
	}{
		{
			name: "t_GetCourier",
			fields: fields{
				courierStorage: mocks.NewCourierStorager(t),
				allowedZone:    mocks2.NewPolygonChecker(t),
				disabledZones:  nil,
			},
			args:    args{ctx: context.Background()},
			want:    &models.Courier{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.courierStorage.On("GetOne", tt.args.ctx).Return(tt.want, nil)
			tt.fields.courierStorage.On("Save", tt.args.ctx, mock.AnythingOfType("models.Courier")).Return(nil)
			tt.fields.allowedZone.On("Contains", mock.Anything).Return(true)

			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			got, err := c.GetCourier(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCourier() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCourier() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierService_MoveCourier(t *testing.T) {
	type fields struct {
		courierStorage *mocks.CourierStorager
		allowedZone    *mocks2.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		courier   models.Courier
		direction int
		zoom      int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "",
			fields: fields{
				courierStorage: mocks.NewCourierStorager(t),
				allowedZone:    mocks2.NewPolygonChecker(t),
				disabledZones:  nil,
			},
			args: args{
				courier:   models.Courier{},
				direction: 1,
				zoom:      1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.courierStorage.On("Save", mock.Anything, mock.AnythingOfType("models.Courier")).Return(nil)
			tt.fields.allowedZone.On("Contains", mock.Anything).Return(true)

			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			if err := c.MoveCourier(tt.args.courier, tt.args.direction, tt.args.zoom); (err != nil) != tt.wantErr {
				t.Errorf("MoveCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
