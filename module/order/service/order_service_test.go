package service

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/noleeen/geo_courier/geo"
	mocks2 "gitlab.com/noleeen/geo_courier/geo/mocks"
	"gitlab.com/noleeen/geo_courier/module/order/models"
	"gitlab.com/noleeen/geo_courier/module/order/storage/mocks"
	"reflect"
	"testing"
)

func TestOrderService_GenerateOrder(t *testing.T) {
	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   *mocks2.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "t_GenerateOrder",
			fields: fields{
				storage:       mocks.NewOrderStorager(t),
				allowedZone:   mocks2.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args:    args{ctx: context.Background()},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.storage.On("GenerateUniqueID", mock.Anything).Return(int64(0), nil)
			tt.fields.storage.On("Save", tt.args.ctx, mock.AnythingOfType("models.Order"), mock.Anything).Return(nil)
			tt.fields.allowedZone.On("Contains", mock.Anything).Return(true)
			tt.fields.allowedZone.On("RandomPoint", mock.Anything).Return(geo.Point{})

			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := o.GenerateOrder(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("GenerateOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_GetByRadius(t *testing.T) {
	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   *mocks2.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx    context.Context
		lng    float64
		lat    float64
		radius float64
		unit   string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []models.Order
		wantErr bool
	}{
		{
			name: "t_GetByRadius",
			fields: fields{
				storage:       mocks.NewOrderStorager(t),
				allowedZone:   mocks2.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args: args{
				ctx:    context.Background(),
				lng:    0,
				lat:    0,
				radius: 0,
				unit:   "m",
			},
			want:    []models.Order{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.storage.
				On("GetByRadius", tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit).
				Return(tt.want, nil)

			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			got, err := o.GetByRadius(tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByRadius() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByRadius() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderService_GetCount(t *testing.T) {
	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   *mocks2.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "t_GetCount",
			fields: fields{
				storage:       mocks.NewOrderStorager(t),
				allowedZone:   mocks2.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args:    args{ctx: context.Background()},
			want:    0,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.storage.On("GetCount", tt.args.ctx).Return(tt.want, nil)

			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			got, err := o.GetCount(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCount() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetCount() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderService_RemoveOldOrders(t *testing.T) {
	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   *mocks2.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "t_Remove",
			fields: fields{
				storage:       mocks.NewOrderStorager(t),
				allowedZone:   mocks2.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args:    args{ctx: context.Background()},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.storage.On("RemoveOldOrders", tt.args.ctx, mock.Anything).Return(nil)

			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := o.RemoveOldOrders(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("RemoveOldOrders() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_Save(t *testing.T) {
	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   *mocks2.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx   context.Context
		order models.Order
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "t_Save",
			fields: fields{
				storage:       mocks.NewOrderStorager(t),
				allowedZone:   mocks2.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args: args{
				ctx:   context.Background(),
				order: models.Order{},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.storage.On("Save", tt.args.ctx, tt.args.order, mock.Anything).Return(nil)

			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := o.Save(tt.args.ctx, tt.args.order); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
